package main

type VertexConcrete struct {
	Uid          string  `json:"uid"`
	Name         string  `json:"name"`
	End          bool    `json:"end"`
	Type         string  `json:"type"`
	Node         string  `json:"node"`
	Ip           string  `json:"ip"`
	Speedup      float64 `json:"speedup"`
	Availability float64 `json:"availability"`
}

type VertexesConcrete []VertexConcrete

type ConcreteMap struct {
	Uid      string                 `json:"uid"`
	Services VertexesConcrete       `json:"services"`
	Joines   JoinesAbstractConcrete `json:"join"`
	TMax     float64                `json:"tmax"`
	TMin     float64                `json:"tmin"`
	LogAMax  float64                `json:"logamax"`
	LogAMin  float64                `json:"logamin"`
	R        float64                `json:"r"`
	LogA     float64                `json:"loga"`
}

type JoinAbstractConcrete struct {
	Uid             string   `json:"uid"`
	Node            string   `json:"node"`
	Ip              string   `json:"ip"`
	Speedup         float64  `json:"speedup"`
	Availability    float64  `json:"availability"`
	Name            string   `json:"name"`   // nome del servizio concreto (e lui sceglie la replica)
	Method          string   `json:"method"` // GET/POST/PUT/DELETE
	Url             string   `json:"url"`    //Uid    string   `json:"uid"`
	Order           string   `json:"order"`  // Ordine con cui deve essere eseguita la chiamata es. 1, 2, 3...
	To              []string `json:"to"`     // A chi il vertex passa il suo output.. in questo caso potrei pensare a indicate il "name" astratto (Forse si può dedurre questo campo dagli archi in realtà...!!!)
	ToIp            []string `json:"toip"`
	ToMicroservices []string `json:"tomicroservices"`
	IsToUrlConcat   []string `json:"istourlconcat"`
	ToUrlConcat     []string `json:"tourlconcat"`
	IsToParameters  []string `json:"istoparameters"`
	ToParameters    []string `json:"toparameters"`
	Microservice    string   `json:"microservice"` // E' il nome del microservizio invocato su gokit
	IsBody          bool     `json:"isbody"`       //
	Body            string   `json:"body"`         // json body in rappresentazione stringa..
	IsParameters    bool     `json:"isparameters"` //
	Parameters      []string `json:"parameters"`   // Eventuali parametri da passare all' url
	IsUrlConcat     bool     `json:"isurlconcat"`  //
	UrlConcat       []string `json:"urlconcat"`    // Eventuali parametri da passare su linea
	End             bool     `json:"end"`          // campo che forse è inutile
	Type            string   `json:"type"`         // (if/else condizionale) oppure { "microservice", "???"  }
	IsBound         bool     `json:"isbound"`      // sarà da settare a True se va a essere mappato su un service concreto
	BoundTo         string   `json:"boundto"`      // corrisponde al service che salta fuori durante la risoluzione del grafo
}

func makeJoinAbstractConcrete(
	uid string,
	node string,
	ip string,
	speedup float64,
	availability float64,
	name string,
	method string,
	url string,
	order string,
	to []string,
	toip []string,
	tomicroservices []string,
	istourlconcat []string,
	tourlconcat []string,
	istoparameters []string,
	toparameters []string,
	microservice string,
	isBody bool,
	body string,
	isParameters bool,
	parameters []string,
	isUrlConcat bool,
	urlConcat []string,
	end bool,
	t string,
	isBound bool,
	boundTo string) JoinAbstractConcrete {
	return JoinAbstractConcrete{
		Uid:             uid,
		Node:            node,
		Ip:              ip,
		Speedup:         speedup,
		Availability:    availability,
		Name:            name,
		Method:          method,
		Url:             url,
		Order:           order,
		To:              to,
		ToIp:            toip,
		ToMicroservices: tomicroservices,
		IsToUrlConcat:   istourlconcat,
		ToUrlConcat:     tourlconcat,
		IsToParameters:  istoparameters,
		ToParameters:    toparameters,
		Microservice:    microservice,
		IsBody:          isBody,
		Body:            body,
		IsParameters:    isParameters,
		Parameters:      parameters,
		IsUrlConcat:     isUrlConcat,
		UrlConcat:       urlConcat,
		End:             end,
		Type:            t,
		IsBound:         isBound,
		BoundTo:         boundTo,
	}
}

type JoinesAbstractConcrete []JoinAbstractConcrete
