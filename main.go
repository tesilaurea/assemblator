package main

import (
	"fmt"
	"github.com/gorilla/handlers"
	"log"
	"net/http"
	_ "os"
)

/**
 * Main principale del client backend scritto in Go
 */

func main() {
	pathToOptimizer = "http://localhost:8080/optimal/submit/workflow"
	clientWrite = "http://localhost:8081/join/write"
	clientGet = "http://localhost:8081/join/get/"
	fmt.Println("Tutto funziona correttamente")
	port := ":8082"
	router := NewRouter()
	log.Fatal(http.ListenAndServe(port, handlers.CORS()(router)))

}
