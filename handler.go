package main

import (
	_ "bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	_ "log"
	"net/http"
	_ "strconv"
	_ "time"
)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Ricezione abstract workflow
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Posso pensare di restituire			201 se tutto è ok
//Posso pensare di restituire			404 se è indisponibile, ovvero si sta ancora assemblando

func GetAssemblatedWorkflow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uid := vars["uid"]
	concreteMap := GetConcreteMap(uid)
	jList := concreteMap.Joines
	fmt.Println(jList)
	httpServiceList := JoinesToHttpServiceList(jList)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK) //httpServiceList
	if err := json.NewEncoder(w).Encode(httpServiceList); err != nil {
		panic(err)
	}
}

func PostAssemblateWorkflow(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uid := vars["uid"]

	body, err := ioutil.ReadAll(r.Body)
	//var workflow Workflow

	defer r.Body.Close()
	if err != nil {
		panic(err)
	}

	//----------------------------------------

	//devo modificare l'ottimizzatore

	concreteMap := PostToOptimizer(body)

	concreteMap.Uid = uid
	SaveConcreteMap(concreteMap)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(concreteMap); err != nil {
		panic(err)
	}

	// dovrei provare a passare da concreteMap a una forma che vada bene per un ipotetico apigateway per
	// poi poter esporre il service

	// Mi aspetto che sia possibile fare il mapping servizi astratti e servizi concreti

	// in teoria dal concrete dovrei passare ad una lista di questi qui che però sono una unità più piccola del Pod, perché
	// modellano l'effettiva chiamata che devo fare! Nel senso che il pod per me è il Service concreto.. mentre un HttpService
	// mi va a modellare l'effettivo microservizio/endpoint che invoco

	// ovviamente dire che un pod è "Service1" vuol dire che ha un certo "Method" e un certo "Url".. Diciamo che in teoria per
	// noi Method è GET/POST/PUT/DELETE che però non sono un problema per il caso di gokit, ma magari si fa la distinzione in
	// altri casi, soprattutto se si vuole integrare altri servizi che non siano ad esempio microservizi (posso pensare di
	// eseguire in quel caso un immagine gokit con un podesterno o ammettere che lui esponga una getEcho etc... )

	/*type HttpService struct {
		Method string   `json:"method"`    // io credo che questa info sia semplicemente espressa dall' utente all'inizio
		Body   string   `json:"body"`      // il body credo che se ci fosse sia indicato dall'utente esplicitamente,
											  ad esempio l'utente potrebbe dire:
											  - il body è questo Json qui
											  - il body viene facendo comunque una get attraverso un intermediario e poi
											    caricandolo comunque su un db per injection (in teoria è aperta la possibilità
											    stessa di caricare anche il dato dal sensore)
		Url    string   `json:"url"`	   // questa è specificata da concreteMap
		Uid    string   `json:"uid"`	   // questo è l'id univoco del servizio Concreto definito a priori
		Level  string   `json:"level"`     // questo è il livello di annidamento dei servizi, e specifica l'ordine che deve essere eseguito
										   // nel chiamare e viene specificato in fase di creazione del grafo astratto
		To     []string `json:"to"`        // questo campo viene sempre dalla definizione iniziale che fa l'utente del grafo astratto
	}*/

	// N.B GESTIRE IL CASO DELLE SOURCES

	//Tecnicamente invece di ritornare la cosa al consumer deploy, dovrei salvarlo in qualche db... ? MongoDB??
	//Con il relativo id di servizio
}
