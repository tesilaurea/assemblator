package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// QUI DEVO PASSARE DA UNA RAPPRESENTAZIONE TIPO SALVATA SU MONGODB A QUELLA
// RELATIVA ALLE CHIAMATE CHE DEVONO ESSERE ESEGUITE DALL' API GATEWAY

func JoinesToHttpServiceList(jList JoinesAbstractConcrete) []HttpService {
	var httpServiceList []HttpService //:= make([]HttpService, len(jList))
	for _, j := range jList {

		url := "http://" + j.Ip + "/"
		microservice := j.Microservice

		url = url + microservice
		body := ""
		method := j.Method
		toip := j.ToIp
		tomicroservices := j.ToMicroservices

		istourlconcat := j.IsToUrlConcat
		tourlconcat := j.ToUrlConcat
		istoparameters := j.IsToParameters
		toparameters := j.ToParameters

		// Sostituisco eventuali parametri sull'url
		if j.IsUrlConcat {
			urlConcat := j.UrlConcat
			fmt.Println(url)
			url = replaceConcatParams(url, urlConcat)

		}
		// Allego parametri sull'url
		if j.IsParameters {
			parameters := j.Parameters
			url = addParameters(url, parameters)
		}
		// Se c' è un body esterno da spedire lo prendo da qui (utile nel caso di primo servizio contattato)
		if j.IsBody {
			body = j.Body
		}
		// Vanno aggiunti dei controlli per definire to
		//
		fmt.Println(toip)
		fmt.Println(tomicroservices)
		fmt.Println(istourlconcat)
		fmt.Println(tourlconcat)
		fmt.Println(istoparameters)
		fmt.Println(toparameters)
		toip = UpdateToIp(jList, toip)

		to := ConstructDestUrls(tomicroservices,
			toip, istourlconcat, tourlconcat, istoparameters, toparameters)

		httpService := makeHttpService(method, body, url, j.Name, j.Order, to, j.To)
		httpServiceList = append(httpServiceList, httpService)
	}
	return httpServiceList
}

func GetConcreteMap(uid string) ConcreteMap {
	var concreteMap ConcreteMap
	resp, err := http.Get(clientGet + uid)
	if err != nil {
		// handle error
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &concreteMap); err != nil {
		panic(err)
	}
	return concreteMap
}

// Invoco il risolutore
func PostToOptimizer(body []byte) ConcreteMap {
	var concreteMap ConcreteMap
	resp, err := http.Post(pathToOptimizer, "application/json; charset=UTF-8", bytes.NewBuffer(body))
	if err != nil {
		// handle error
		panic(err)
	}
	body, err = ioutil.ReadAll(resp.Body)
	if err := json.Unmarshal(body, &concreteMap); err != nil {
		//w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		//w.WriteHeader(422) // unprocessable entity
		//if err := json.NewEncoder(w).Encode(err); err != nil {
		//	panic(err)
		//}
		panic(err)
	}
	return concreteMap
}

//Salvo su mongoDB
func SaveConcreteMap(concreteMap ConcreteMap) {

	_, err := http.Post(clientWrite, "application/json; charset=UTF-8", bytes.NewBuffer(StructToJson(concreteMap)))
	if err != nil {
		// handle error
		panic(err)
	}
}
